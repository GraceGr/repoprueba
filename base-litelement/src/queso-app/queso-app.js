import { LitElement, html } from 'lit-element';
import './queso-header.js';
import './queso-main.js';
import './queso-sidebar.js';
import './queso-footer.js';

class QuesoApp extends LitElement {
	
	static get properties() {
		return {
			
		};
	}

	
	constructor() {
		super();	

	}		

	
	render() {
		return html`
			<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<div>
				<queso-header class="text-center"></queso-header>
			</div>
			<div class="row r-3">
				
				<queso-main @cheeses-updated=${this.cheesesUpdated} class="col-10"></queso-main>
			</div>
			<div>
				<queso-footer class="text-center"></queso-footer>
			</div>
	`;
  }
  
  newCheese(e) {
	this.shadowRoot.querySelector("queso-main").showCheeseData = false; 	  
}
  
}

customElements.define('queso-app', QuesoApp)  