import { LitElement, html } from 'lit-element';

class QuesoFooter extends LitElement {
	
	static get properties() {
		return {			
		};
	}
	
	constructor() {
		super();
	}
	
  render() {
    return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
                          <footer class="bg-dark text-center text-white fixed-bottom">
                                <div>
                                   <h2>Quesos 2021</h2>
                                </div>
	                  </footer>			
	`;
  }    
}

customElements.define('queso-footer', QuesoFooter)