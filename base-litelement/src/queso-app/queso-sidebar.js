import { LitElement, html } from 'lit-element';

class QuesoSidebar extends LitElement {
	
	static get properties() {
		return {
			cheeses: {type: Array}
		};
	}
	
	constructor() {
		super();		

		this.cheeses = this.cheeses || [];
	}
	
  render() {
    return html`	
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<section>
				<div>
					
				<div>
				<div class="mt-5">
					<center>
						<button class="w-200 btn bg-success" style="font-size: 12
						px" @click="${this.newCheese}"><strong>AGREGA</strong></button>								
					</center>
					
				<div>				
			</section>
		
	`;
  }
  
  newCheese(e) {

	  this.dispatchEvent(new CustomEvent("new-cheese", {})); 
  }
}

customElements.define('queso-sidebar', QuesoSidebar)