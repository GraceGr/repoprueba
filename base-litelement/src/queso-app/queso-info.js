import { LitElement, html } from 'lit-element';

class QuesoInfo extends LitElement {
	
	static get properties() {
		return {
            cheese: {type: Object},
			newCheese: {type: Boolean}
		};
	}
	
	constructor() {
        super();
        
            this.cheese = this.cheese || {};
		    this.cheese.name = this.cheese.name || "";		
		    this.cheese.name = this.cheese.description || "";
		    this.newCheese = false;
		    this.cheese.image = {
				"src": "./img/Caerphilly_cheese.jpg",
				"height": 100,
				"width": 50
			};     
        
	}
	
  render() {
    return html`
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<div class='border rounded border-secondary'>
			
			<div>
        			<h4><center><b>Datos Queso<b></center></h4>
    		</div>
			
			<div class='border'>
				<form>
					<div class="form-group">
						<label>Nombre Queso</label>
						<input type="text" id="cheeseFormName"  class="form-control" placeholder="Nombre" .value="${this.cheese.name}" @input="${(e) => this.cheese.name = e.target.value}"/>
					</div>
					<div class="form-group">
						<label>Descripción</label>
						<textarea class="form-control" placeholder="Características del queso" rows="2" .value="${this.cheese.description}" @input="${(e) => this.cheese.description = e.target.value}"></textarea>
                    </div>
                    <div class="form-group">
						<label>Recomendación</label>
						<input type=Boolean class="form-control" placeholder="Recomendación" .value="${this.cheese.recommended}" @input="${(e) => this.cheese.recommended = e.target.value}">
                    </div>
					<div class="form-group">
						<label>Maduración</label>
						<input type="text" class="form-control" placeholder="En meses" .value="${this.cheese.age ? this.cheese.age : "" }" @input="${(e) => this.cheese.age = e.target.value}"/>			
					</div>
				</form>
			</div>

            <div class="modal-footer ">
  	                      <button type="submit" class="btn btn-success" @click=${this.goBack} ><strong>Regresa</strong></button>
                         <button type="submit" @click=${this.storeInfo} class="btn btn-success"><strong>Guarda</strong></button>
                        </div>
			
		</div>
	`;
}

goBack(e) {
    console.log("goBack");	  
    e.preventDefault();	
    this.cheese = {};
    this.dispatchEvent(new CustomEvent("info-cheese-close",{}));
    this.shadowRoot.getElementById("cheeseFormName").disabled = true;
}

storeInfo(e) {
    console.log("Entro a storeInfo")
    let micheese={detail: {cheese: {name:this.cheese.name, description:this.cheese.description, age:this.cheese.age, image:this.cheese.image }}};
    console.log(micheese);
    this.dispatchEvent(new CustomEvent("info-cheese-store", micheese));



}

updateName(e) {
    console.log("updateName");
    console.log("Actualizando la propiedad nombre  con el valor " + e.target.value);
    this.cheeses.name= e.target.value;
}

updateDescription(e) {
    console.log("updateDescription");
    console.log("Actualizando la propiedad descripcion con el valor " + e.target.value);
    this.cheeses.description = e.target.value;
}

updateAge(e) {
    console.log("updateAge");
    console.log("Actualizando la propiedad age con el valor " + e.target.value);
    this.cheeses.age = e.target.value;
}

updateRecommended(e) {
    console.log("updateRecommended");
    console.log("Actualizando la propiedad recommended con el valor " + e.target.value);
    this.cheeses.recommended = e.target.value;
}

guardaCheese(e) {
    console.log("guardaCheeses");
    
        
    console.log("La propiedad name vale " + this.cheeses.name);
    console.log("La propiedad description vale " + this.cheeses.description);
    console.log("La propiedad age vale " + this.cheeses.age);
        
    this.dispatchEvent(new CustomEvent("queso-form-store",{
        detail: {
            cheeses:  {
                    name: this.cheeses.name,
                    description: this.cheeses.description,
                    age: this.cheeses.age,
                    photo: this.cheeses.image
                }
            }
        })
    );
}
   


}


    


customElements.define('queso-info', QuesoInfo)