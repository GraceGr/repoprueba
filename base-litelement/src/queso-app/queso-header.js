import { LitElement, html } from 'lit-element';

class QuesoHeader extends LitElement {
	
	static get properties() {
		return {			
		};
	}
	
	constructor() {
		super();			
	}
	
  render() {
    return html`
		      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
          </header class="bg-dark text-center text-white fixed-bottom">
          <div>
          <h1>Menu de Quesos</h1>
        </div>	
        </header>
	`;
  }    
}

customElements.define('queso-header', QuesoHeader)